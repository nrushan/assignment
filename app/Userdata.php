<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Userdata extends Model
{
    protected $table = 'csv_data';
    public $timestamps = false;

    
    
    /**
     * 
     * @param type $start
     * @param type $end
     * @return type
     * 
     * returns cohort data
     */
    public function cohort_data($start,$end){
    
        $user = new Userdata ();
        
//        get user count
        $user_count=$user::get()->count(); 
                
        $data = $user::groupBy('onboarding_percentage')
                ->select('onboarding_percentage', DB::raw('count(*) as total'))
                ->where([
                        ['created_at','>=',$start],
                        ['created_at','<=',$end]
                    ])
                ->get();
        
        $result_arr=array("20"=>0,"40"=>0,"50"=>0,"70"=>0,"90"=>0,"99"=>0,"100"=>0);
        
        foreach ($data as $row){
    
//            echo $row->onboarding_percentage.' '.$row->total.'<br>';
            
            switch($row->onboarding_percentage){
                
//                case '0':
//                    $result_arr[$row->onboarding_percentage]=$row->total;
//                    break;
                
                case '20':
                    $result_arr[$row->onboarding_percentage]=round(($row->total/$user_count)*100);
                    break;
                
                case '40':
                    $result_arr[$row->onboarding_percentage]=round(($row->total/$user_count)*100);
                    break;
                
                case '50':
                    $result_arr[$row->onboarding_percentage]=round(($row->total/$user_count)*100);
                    break;
                
                case '70':
                    $result_arr[$row->onboarding_percentage]=round(($row->total/$user_count)*100);
                    break;
                
                case '90':
                    $result_arr[$row->onboarding_percentage]=round(($row->total/$user_count)*100);
                    break;
                
                case '99':
                    $result_arr[$row->onboarding_percentage]=round(($row->total/$user_count)*100);
                    break;
                
                case '100':
                    $result_arr[$row->onboarding_percentage]=round(($row->total/$user_count)*100);
                    break;
            }
        }

        return $result_arr;
    }
}
