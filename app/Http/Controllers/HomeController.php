<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Chart;
use App\Userdata;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    


    public function index()
    {

        $user = new Userdata();

        /*
         * Get first and last record from the table
         */
        
        $record_first=$user::orderBy('created_at', 'asc')->first();
        
        $record_last =$user::orderBy('created_at', 'desc')->first();
        

        $start_date =$record_first->created_at;
        $end_date   =$record_last->created_at;

        $date=strtotime($start_date);
        $i=1;
        
        $chart=array();
       
        /*
         * get cohort data for the given data range
         */
        while ($date <= strtotime($end_date)){
            $result=array();
            $start  =date('Y-m-d',$date);
            $end    =date('Y-m-d',$date+(86400*6));
            
            $result[$i]['name']   ="Week ".$i;
            $result[$i]['data']   =array_values($user->cohort_data($start,$end));
            
            $chart[]=$result[$i];
            
            $date =$date+(86400*7);
            $i++;
        }
                    

              
        /*
         * chart configurations
         */       
        $charts = [
                    'chart' => ['type' => 'line'],
                    'title' => ['text' => 'Retention Chart'],
                    'xAxis'=> [
                        'title'=>[
                            'text' => 'Onboarding Step'
                         ],
                        'labels'=>[
                            'align' => 'left'
                        ],
                        'categories'=> ['20%', '40%', '50%', '70%', '90%', '99%', '100%']
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => 'Number of Users%'
                        ],
                    ],
                    'series' => $chart,
                    'plotOptions'=> [
                        'line'=> [
                            'dataLabels'=> [
                                'enabled'=> false,
                            ],
                            'enableMouseTracking'=> false
                        ]
                    ],
                ];  

        
        return view('home',compact('charts'));
      
    }
    
    /*
     * returns number of weeks
     */
    function week_between_two_dates($date_start, $date_end)
    {
        $first = DateTime::createFromFormat('m/d/Y', $date_start);
        $second = DateTime::createFromFormat('m/d/Y', $date_end);
        
        if($date_start > $date_end) 
            return $this->week_between_two_dates($date_start, $date_end);
        
        return floor($first->diff($second)->days/7);
    }

}
