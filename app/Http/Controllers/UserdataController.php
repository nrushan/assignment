<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdata;


class UserdataController extends Controller
{
    
    public function save_data(){
        
         $flag = true;
        
         if (($handle = fopen ( public_path () . '/export.csv', 'r' )) !== FALSE) {
         
             while ( ($data = fgetcsv ( $handle, 1000, ';' )) !== FALSE ) {
             
                if($flag) { $flag = false; continue; }
                
                $csv_data = new Userdata ();
                $csv_data->user_id                      = $data [0];
                $csv_data->created_at                   = $data [1];
                $csv_data->onboarding_percentage        = ($data [2])?$data [2]:0;
                $csv_data->count_applications           = $data [3];
                $csv_data->count_accepted_applications  = $data [4];
                $csv_data->save ();
            }
            fclose ( $handle );
            
            echo "Successfully imported";
        }
    }
    
    
    public function get_data(){
        
        $csv_data = new Userdata ();
        
        $data = $csv_data::all ();
        
//        var_dump($data);
    }
    
    
    
}
