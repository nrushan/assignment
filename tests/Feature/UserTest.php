<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Userdata;
use DB;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        
        $user = new Userdata ();
        
        $data = $user::groupBy('onboarding_percentage')
                ->select('onboarding_percentage', DB::raw('count(*) as total'))
                ->where([
                        ['created_at','>=',"2016-07-19"],
                        ['created_at','<=',"2016-08-10"]
                    ])
                ->get();
        
        
        
        print_r($data);
        
        $this->assertTrue(true);
    }
}
